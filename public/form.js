$('document').ready(function(){
    $('.pi').on('click', function(e){
        e.preventDefault();
        let href = $(this).attr('href');
        getContent(href, true);
    });

    $('.close').click(function(){
         $('.zv_up').fadeOut();
         $('main').css('filter','none');
         window.history.back();
      });
});

window.addEventListener("popstate", function(e) {
    getContent(location.pathname, false);
});


function getContent(url, addEntry) {
        if(addEntry === true) {
         $('.zv_up').fadeIn();
         $('main').css('filter','blur(5px)');
         window.history.pushState({page:1}, null, '?#form_zvon');}
         else {
             $('.zv_up').fadeOut();
             $('main').css('filter','none');
             window.history.replaceState({page:0},null,'https://veronika.kholudeeva.gitlab.io/web10/');
         }
        }
